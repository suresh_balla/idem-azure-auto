from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
RESOURCE_PARAMETERS = {"location": "eastus"}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of resource group. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.resource_management.resource_groups.present = (
        hub.states.azure.resource_management.resource_groups.present
    )
    mock_hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present = (
        hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present
    )
    mock_hub.tool.azure.resource_management.conversion_utils.convert_present_to_raw_resource_group = (
        hub.tool.azure.resource_management.conversion_utils.convert_present_to_raw_resource_group
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}",
            "name": RESOURCE_NAME,
            "location": "eastus",
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.resource_management.resource_groups",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.resource_management.resource_groups.present(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, **RESOURCE_PARAMETERS
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Created azure.resource_management.resource_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of resource group. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.resource_management.resource_groups.present = (
        hub.states.azure.resource_management.resource_groups.present
    )
    mock_hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present = (
        hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present
    )
    mock_hub.tool.azure.resource_management.resource_groups.update_resource_groups_payload = (
        hub.tool.azure.resource_management.resource_groups.update_resource_groups_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
    }

    resource_parameters_update_raw = {
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}",
            "name": RESOURCE_NAME,
            "location": "eastus",
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.resource_management.resource_groups.present(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, **resource_parameters_update
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Updated azure.resource_management.resource_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of resource group. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.resource_management.resource_groups.absent = (
        hub.states.azure.resource_management.resource_groups.absent
    )
    mock_hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present = (
        hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.resource_management.resource_groups.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.resource_management.resource_groups '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of resource group. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.resource_management.resource_groups.absent = (
        hub.states.azure.resource_management.resource_groups.absent
    )
    mock_hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present = (
        hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}",
            "name": RESOURCE_NAME,
            "location": "eastus",
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.resource_management.resource_groups.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.resource_management.resource_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of resource group.
    """
    mock_hub.states.azure.resource_management.resource_groups.describe = (
        hub.states.azure.resource_management.resource_groups.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present = (
        hub.tool.azure.resource_management.conversion_utils.convert_raw_resource_group_to_present
    )
    resource_id = f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
    expected_list = {
        "ret": {
            "value": [{"id": resource_id, "name": RESOURCE_NAME, "location": "eastus"}]
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.resource_management.resource_groups.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.resource_management.resource_groups.present" in ret_value.keys()
    described_resource = ret_value.get(
        "azure.resource_management.resource_groups.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state.get("tags") == old_state.get("tags")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state.get("tags") == new_state.get("tags")
